import '../components/infoproduk.css'


// const mystyle = {

//     boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)"

// };


const InfoProduk = () => {


    return (

        <>

            {/* <nav className="navbar navbar-expand-lg navbar-light fixed-top" style={mystyle}>

                <div className="container">

                    <a className="navbar-brand me-auto" id="logo" href="/">&nbsp;</a>

                </div>

            </nav> */}
<br></br>
<br></br>
<br></br>
            <div className='container'><br />

                <div className='row mt-5'>

                    <div className='col-md-3'>

                        <a href='/'><i className="bi bi-arrow-left offset-md-5"></i></a>

                    </div>

                    <form action='#' className='col-md-6'>

                        <div className="col-md mb-3">

                            <label htmlFor="nm_produk" className="form-label">Nama Produk</label>

                            <input type="type" className="form-control" id="nm_produk" placeholder="Nama Produk" />

                        </div>

                        <div className="col-md mb-3">

                            <label htmlFor="harga_produk" className="form-label">Harga Produk</label>

                            <input type="type" className="form-control" id="harga_produk" placeholder="Rp 0,00" />

                        </div>

                        <div className="col-md mb-3">

                            <label htmlFor="kategori" className="form-label">Kategori</label>

                            <select class="form-control" id="kategori">

                                <option>Pilih Kategori</option>

                                <option>1</option>

                                <option>2</option>

                                <option>3</option>

                                <option>4</option>

                            </select>

                        </div>

                        <div className="col-md mb-3">

                            <label htmlFor="deskripsi" className="form-label">Harga Produk</label>

                            <textarea class="form-control" id="deskripsi" rows="3" placeholder='Contoh: Jalan Ikan Hiu 33'></textarea>

                        </div>

                        <div className="col-md mb-3">

                            <label htmlFor="foto" className="form-label">Foto Produk</label><br />

                            <label><img src='/assets/img/Group_1.png' alt='' /><input type={'file'} hidden /></label>
                            <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" fill="currentColor" class="bi bi-plus-square-dotted" viewBox="0 0 16 16">
                            <path d="M2.5 0c-.166 0-.33.016-.487.048l.194.98A1.51 1.51 0 0 1 2.5 1h.458V0H2.5zm2.292 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zm1.833 0h-.916v1h.916V0zm1.834 0h-.917v1h.917V0zm1.833 0h-.917v1h.917V0zM13.5 0h-.458v1h.458c.1 0 .199.01.293.029l.194-.981A2.51 2.51 0 0 0 13.5 0zm2.079 1.11a2.511 2.511 0 0 0-.69-.689l-.556.831c.164.11.305.251.415.415l.83-.556zM1.11.421a2.511 2.511 0 0 0-.689.69l.831.556c.11-.164.251-.305.415-.415L1.11.422zM16 2.5c0-.166-.016-.33-.048-.487l-.98.194c.018.094.028.192.028.293v.458h1V2.5zM.048 2.013A2.51 2.51 0 0 0 0 2.5v.458h1V2.5c0-.1.01-.199.029-.293l-.981-.194zM0 3.875v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 5.708v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zM0 7.542v.916h1v-.916H0zm15 .916h1v-.916h-1v.916zM0 9.375v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .916v.917h1v-.917H0zm16 .917v-.917h-1v.917h1zm-16 .917v.458c0 .166.016.33.048.487l.98-.194A1.51 1.51 0 0 1 1 13.5v-.458H0zm16 .458v-.458h-1v.458c0 .1-.01.199-.029.293l.981.194c.032-.158.048-.32.048-.487zM.421 14.89c.183.272.417.506.69.689l.556-.831a1.51 1.51 0 0 1-.415-.415l-.83.556zm14.469.689c.272-.183.506-.417.689-.69l-.831-.556c-.11.164-.251.305-.415.415l.556.83zm-12.877.373c.158.032.32.048.487.048h.458v-1H2.5c-.1 0-.199-.01-.293-.029l-.194.981zM13.5 16c.166 0 .33-.016.487-.048l-.194-.98A1.51 1.51 0 0 1 13.5 15h-.458v1h.458zm-9.625 0h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zm1.834-1v1h.916v-1h-.916zm1.833 1h.917v-1h-.917v1zm1.833 0h.917v-1h-.917v1zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                        </svg>
                        </div>

                        <div className='row '>

                            <div className="col-md mb-3 d-grid Preview">

                                <a href='#/' className="btn btn-outline-primary">Preview</a>

                            </div>

                            <div className="col-md mb-3 d-grid Terbitkan ">

                                <button type="submit" className="btn btn-primary">Terbitkan</button>

                            </div>

                        </div>

                    </form>

                </div>

            </div>

        </>


    )

}


export default InfoProduk
