// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import '@fontsource/poppins';
// import "./css/file.css"
import { Button } from "react-bootstrap";
// import {
//   Container,
//   Form,
//   // Modal
// } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AvatarImageCropper from 'react-avatar-image-cropper';


export default function profile() {
  const setImage = (file) => {
    console.log(file)
  }

  const actions = [
    <Button variant="info" key={0}>Apply</Button>,
    <Button variant="dark" key={1}>Cancel</Button>,
  ]

  return (
    <>
     {/* <Nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <div class="container-fluid">  </div>
        <a class="navbar-brand brand" style={{backgroundColor: 'blueviolet', marginRight: 1100, width: 1500}} href="/">&nbsp;</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight"
                aria-controls="offcanvasRight" aria-expanded="false" aria-label="Toggle navigation" >
            <span class="navbar-toggler-icon" ></span>
                </button>
          <div class="offcanvas offcanvas-end w-50" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
              <div class="offcanvas-header">

                <h5 id="offcanvasRightLabel">BCR</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
              <div class="text-center">
                <h6 style={{marginRight: 550, marginTop: 25}}>Lengkapi Info Akun</h6>
              </div>
          </div>
          </Nav> */}
   
      <br></br>
      <br></br>
      <br></br>

      <div className='container'><br />
      <AvatarImageCropper apply={setImage} actions={actions} />
<div className='row mt-5'>

    <div className='col-md-3'>

        <a href='/'><i className="bi bi-arrow-left offset-md-5"></i></a>

    </div>

    <form action='#' className='col-md-6'>

        <div className="col-md mb-3">

            <label htmlFor="nm_produk" className="form-label">Nama*</label>

            <input type="type" className="form-control" id="nm_produk" placeholder="Nama" />

        </div>

        <div className="col-md mb-3">

            <label htmlFor="kategori" className="form-label">Kota*</label>

            <select class="form-control" id="kota">

                <option>Pilih kota</option>

                <option>Jakarta</option>

                <option>Bandung</option>

                <option>Bali</option>

                <option>Jambi</option>

            </select>

        </div>

        <div className="col-md mb-3">

            <label htmlFor="deskripsi" className="form-label">Alamat*</label>

            <textarea class="form-control" id="deskripsi" rows="3" placeholder='Contoh: Jalan Ikan Hiu 33'></textarea>

        </div>

        <div className="col-md mb-3">

            <label htmlFor="nm_produk" className="form-label">No.Handphone* </label>

            <input type="type" className="form-control" id="nm_produk" placeholder="+62854263762" />

        </div>

        {/* <div className="col-md mb-3">

            <label htmlFor="foto" className="form-label">Foto Produk</label><br />

            <label><img src='/assets/img/Group_1.png' alt='' /><input type={'file'} hidden /></label>

        </div> */}

        <div className='row'>

            <div className="col-md mb-3 d-grid">

                <button type="submit" className="btn btn-primary">submit</button>

            </div>

        </div>

    </form>

</div>

</div>

</>


)

}
